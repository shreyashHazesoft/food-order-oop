<?php
include "../model/change-password.php";
include "./validation-controller.php";
include "../helper/session-helper.php";

class ChangePasswordController
{
    public $id;
    public $current_password;
    public $newPassword;
    public $confirmPassword;
    public $checkPass;

    public function __construct()
    {
        $this->checkPass = new ChangePassword();
        $this->validationCheck = new ValidationCheck();
    }

    public function changePassword()
    {
        if (isset($_POST["submit"])) {
            $this->id = $_POST["id"];
            $this->current_password = md5($_POST["current_password"]);
            $this->newPassword = md5($_POST["newPassword"]);
            $this->confirmPassword = md5($_POST["confirmPassword"]);
            $res = $this->checkPass->CheckPassword($this->id, $this->current_password);

            if ($res == true) {
                $this->checkPass->updatePassword($this->id, $this->newPassword);
            }
        }
    }
}

$changePassword = new ChangePasswordController();
$changePassword->changePassword();
?>