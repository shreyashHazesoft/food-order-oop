<?php
include "../model/update-admin.php";
include "validation-controller.php";
include "../helper/session-helper.php";

class UpdateAdminController extends ValidationCheck
{
    public $fullname;
    public $username;
    public $email;
    public $updateAdmin;
    public $id;

    public function __construct()
    {
        $this->updateAdmin = new UpdateAdminDB();
        $this->validationCheck = new ValidationCheck();
    }

    public function getAdmindata($id): array
    {
        $res = $this->updateAdmin->giveAdminData($id);
        if ($res == true) {
            $count = mysqli_num_rows($res);
            if($count == 1) {
                $row = mysqli_fetch_assoc($res);
                return $row;
            } else {
                header("location:manage-admin.php");
            }
        }
    }

    public function checkUpdateAdmindata(): void
    {
        if (isset($_POST["update"])) {
            $this->fullname = $_POST["full_name"];
            $this->username = $_POST["user_name"];
            $this->email = $_POST["email"];
            $this->id = $_POST["id"];

            if (!$this->emptyInputForUpdateAdmin($this->fullname, $this->username, $this->email)) {
                flash("update-admin", "Please fill the input");
                header("location:../view/update-admin.php");
            } elseif (!$this->invalidEmail($this->email)) {
                flash("update-admin", "Please give valid email");
                header("location:../view/update-admin.php");
            } elseif (!$this->invalidUsername($this->username)) {
                flash("update-admin", "Please give valid username");
                header("location:../view/update-admin.php");
            } else {
                $this->updateAdmin->changeAdminData($this->id, $this->fullname, $this->username, $this->email);
                flash("update-admin", "Updated successfully");
                header("location: ../view/manage-admin.php");
            }
        }
    }
}

$adminDataUpdate = new UpdateAdminController();
$adminDataUpdate->checkUpdateAdmindata();
?>