<?php

include "../model/delete-food.php";
include "../helper/session-helper.php";

class DeleteFoodController
{
    public $deletefood;

    public function __construct()
    {
        $this->deletefood = new DeleteFood();
    }

    public function checkFoodAndDelete()
    {
        $deleteRes = $this->deletefood->getFoodAndDelete();

        if ($deleteRes == true) {
            header("location: http://localhost/food-site/food-order-oop/view/manage-food.php");
            flash("delete food", "food Deleted Successfully");
        }
    }
}

$deleteFood = new DeleteFoodController();
$deleteFood->checkFoodAndDelete();