<?php

include "../model/add-admin.php";
include "../helper/session-helper.php";
include "validation-controller.php";

class AddAdminController extends ValidationCheck
{
    public $fullname;
    public $username;
    public $email;
    public $password;
    public $confirmpassword;
    public $addAdmin;
    public $validationCheck;

    public function __construct()
    {
        $this->addAdmin = new AddAdminToDB();
        $this->validationCheck = new ValidationCheck();
    }
    public function addAdmin(): void
    {
        if (isset($_POST["submit"])) {
            $this->fullname = $_POST["full_name"];
            $this->username = $_POST["username"];
            $this->email = $_POST["email"];
            $this->password = $_POST["password"];
            $this->confirmpassword = $_POST["confirm_password"];

            if (!$this->emptyInputForAddAdmin($this->fullname, $this->username, $this->email, $this->password, $this->confirmpassword)) {
                flash("add-admin", "Please fill the input");
                header("location: ../view/add-admin.php");
            } elseif (!$this->checkAdminTaken($this->username, $this->email)) {
                flash("add-admin", "Please use different username or email");
                header("location: ../view/add-admin.php");
            } elseif (!$this->invalidEmail($this->email)) {
                flash("add-admin", "Please give the vaild email");
                header("location: ../view/add-admin.php");
            } elseif (!$this->pwdMatch()) {
                flash("add-admin", "Please match the password");
                header("location: ../view/add-admin.php");
            } elseif (!$this->invalidUsername($this->username)) {
                flash("add-admin", "Please give the vaild username");
                header("location: ../view/add-admin.php");
            } else {
                $this->addAdmin->setAdmin($this->fullname, $this->username, $this->email, $this->password);
                flash("add-admin", "added successfully");
                header("location: ../view/manage-admin.php");
            }
        }

    }
}

(new AddAdminController())->addAdmin();
