<?php include('./partials/menu.php')?>

<div class="main-content">
    <div class="wrapper">
        <h1>Order Food</h1>
        <br>
        <?php
            include "../controller/update-food-controller.php";
            $id = $_GET["id"];
            $getFoodData = new UpdateFoodController();
            $row = $getFoodData->getFooddata($id);
        ?>
        <form action="../controller/order-controller.php" method="POST">
            <?php
                include "../helper/session-helper.php";
                flash("order food");
            ?>
            <table class="tbl-30">
                <tr>
                    <td>
                        Food
                    </td>
                    <td>
                        <input type="text" name="food" value="<?php echo $row["title"] ?>" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        Price
                    </td>
                    <td>
                        <input type="text" name="price" value="<?php echo $row["price"] ?>" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        Quantity
                    </td>
                    <td>
                        <input type="text" name="quantity" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        Full Name
                    </td>
                    <td>
                        <input type="text" name="full_name" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        Contact Number
                    </td>
                    <td>
                        <input type="text" name="number" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        <input type="text" name="email" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        Address
                    </td>
                    <td>
                        <input type="text" name="address" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="submit" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php include 'partials/footer.php'; ?>