<?php
include "../connection.php";

class AdminData extends Connection
{
    public function getAdminData(): object
    {
        $connect = $this->connect();
        $sql = "SELECT * FROM tbl_admin;";
        $res = mysqli_query($connect, $sql);
        return $res;
    }
}