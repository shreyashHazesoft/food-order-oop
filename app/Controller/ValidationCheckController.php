<?php

class ValidationCheckController
{
    public function emptyInputForAddAdmin($fullname = "", $username = "", $email = "", $password = "", $confirmpassword = ""): bool
    {
        $result = null;

        if (empty($fullname) || empty($username) || empty($email) || empty($password) || empty($confirmpassword)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function emptyInputForOrderFood(string $food = "", string $price = "", string $quantity = "", string $full_name = "", string $email = "", string $number = "", string $address = ""): bool
    {
        $result = null;

        if (empty($food) || empty($price) || empty($quantity) || empty($full_name) || empty($email) || empty($number) || empty($address)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function emptyInputForUpdateAdmin($fullname="", $username="", $email=""): bool
    {
        $result = null;

        if (empty($fullname) || empty($username) || empty($email)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function emptyInputForUpdateFood($title="", $description="", $price): bool
    {
        $result = null;

        if (empty($title) || empty($description) || empty($price)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function invalidUsername($username=""): bool
    {
        $result = null;

        if (!preg_match("/^[a-zA-Z0-9]{5,}$/", $username)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function invalidEmail($email=""): bool
    {
        $result = null;

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function invalidPassword($uppercase, $lowercase, $number, $specialChars): bool
    {
        $result = null;

        if (!$uppercase || !$lowercase || !$number || !$specialChars) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function pwdMatch(): bool{
        $result = null;

        if ($this->password !== $this->confirmpassword) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function checkAdminTaken($username="", $email=""): bool
    {
        $result = null;

        if (!$this->addAdmin->checkAdmin($username, $email)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function checkFoodTaken($title=""): bool
    {
        $result = null;

        if (!$this->addFood->checkFood($title)) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }
    // public $validationCheck;
    // public $uppercase;
    // public $lowercase;
    // public $number;
    // public $specialChars;

    // $this->uppercase = preg_match('@[A-Z]@', $this->password);
    // $this->lowercase = preg_match('@[a-z]@', $this->password);
    // $this->number    = preg_match('@[0-9]@', $this->password);
    // $this->specialChars = preg_match('@[^\w]@', $this->password);
}