<?php
include "../Model/UpdateFood.php";
include "ValidationCheckController.php";
include "../helper/session-helper.php";

class UpdateFoodController extends ValidationCheckController
{
    public $title;
    public $description;
    public $price;
    public $updateFood;
    public $id;

    public function __construct()
    {
        $this->updateFood = new UpdateFood();
    }

    public function getFooddata($id): array
    {
        $res = $this->updateFood->giveFoodData($id);
        if ($res == true) {
            $count = mysqli_num_rows($res);
            if($count == 1) {
                $row = mysqli_fetch_assoc($res);
                return $row;
            } else {
                header("location: ". $_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function checkUpdateFooddata(): void
    {
        if (isset($_POST["update"])) {
            $this->title = $_POST["title"];
            $this->description = $_POST["description"];
            $this->price = $_POST["price"];
            $this->id = $_POST["id"];

            if (!$this->emptyInputForUpdateFood($this->title, $this->description, $this->price)) {
                flash("update-food", "Please fill the input");
                header("location:../view/update-food.php");
            } else {
                $this->updateFood->changeFoodData($this->id, $this->title, $this->description, $this->price);
                flash("update-food", "Updated successfully");
                header("location: ../view/manage-food.php");
            }
        }
    }
}

$foodDataUpdate = new UpdateFoodController();
$foodDataUpdate->checkUpdateFooddata();
?>