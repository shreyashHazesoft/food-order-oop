<?php
include "../helper/session-helper.php";
include "../Model/OrderFood.php";
include "ValidationCheckController.php";
class OrderFoodController extends ValidationCheckController
{
    public $id;
    public $food;
    public $price;
    public $quantity;
    public $full_name;
    public $email;
    public $address;
    public $number;
    public $orderFood;
    // public $validationCheck
    // national shrepas traders

    public function __construct()
    {
        $this->orderFood = new OrderFood();
    }

    public function orderFood(): void
    {
        if(isset($_POST["submit"])) {
            $this->food = $_POST["food"];
            $this->price = $_POST["price"];
            $this->quantity = $_POST["quantity"];
            $this->full_name = $_POST["full_name"];
            $this->number = $_POST["number"];
            $this->email = $_POST["email"];
            $this->address = $_POST["address"];
            $this->id = $_POST["id"];

            if (!$this->validationCheck->emptyInputForOrderFood($this->food, $this->price, $this->quantity, $this->full_name, $this->email, $this->number, $this->address)) {
                flash("order food", "Please fill the input");
                header("location: ../view/order-food.php");
            } elseif (!$this->validationCheck->invalidEmail($this->email)) {
                flash("order food", "Please fill the input");
                header("location: ../view/order-food.php");
            } else {
                $this->orderFood->orderFood($this->food, $this->price, $this->quantity, $this->full_name, $this->number, $this->email, $this->address);
                flash("add-food", "added successfully");
                header("location: ../view/manage-food.php");
            }
        }
    }

    public function sendOrderData(): object
    {
        $DbRes = $this->orderFood->getOrderData();
        return $DbRes;
    }
}

(new OrderFoodController())->orderFood();