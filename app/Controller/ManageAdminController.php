<?php
include "../Model/ManageAdmin.php";

class ManageAdminController
{
    public $adminData;
    function __construct()
    {
        $this->adminData = new AdminData;
    }
    public function sendAdmin(): object
    {
        $DbRes = $this->adminData->getAdminData();
        return $DbRes;
    }
}