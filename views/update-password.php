<?php include('./partials/menu.php')?>

<div class="main-content">
    <div class="wrapper">
        <h1>
            Change Password
        </h1>
        <br>
        <?php
            $id = $_GET["id"];
        ?>

        <form action="../app/Controller/ChangePasswordController.php" method="POST">
            <table class="tbl-30">
                <tr>
                    <td>
                        Currrent Password
                    </td>
                    <td>
                        <input type="password" name="current_password" value="" placeholder="Old Password">
                    </td>
                </tr>
                <tr>
                    <td>
                        New Password
                    </td>
                    <td>
                        <input type="password" name="new_password" value="" placeholder="New Password">
                    </td>
                </tr>
                <tr>
                    <td>
                        confirm Password
                    </td>
                    <td>
                        <input type="password" name="confirm_password" value="" placeholder="Confirm Password">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <input type="submit" name="submit" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<?php include('./partials/footer.php')?>