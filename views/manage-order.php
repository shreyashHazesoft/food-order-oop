<?php
    include "./partials/menu.php";
    include "../app/Controller/OrderFoodController.php";
    include "../helper/session-helper.php";
?>
<div class="main-content">
    <div class="wrapper">
        <h1>Managa Food</h1>
        <br>
        <strong><?php echo flash("delete admin") ?></strong>
        <br>
        <a href="add-food.php" class="btn-primary text-deco">Add Food</a>
        <br><br>
        <table class="tbl-full">
            <tr>
                <th>S.N</th>
                <th>Title</th>
                <th>Price</th>
                <th>quantity</th>
                <th>Name</th>
                <th>email</th>
                <th>contact number</th>
                <th>Address</th>
                <th>action</th>
            </tr>
            <?php
                $displayOrder = new OrderFoodController();
                $orderData = $displayOrder->sendOrderData();
                $sn=1;
                // print_r($foodData);
                while ($rows = mysqli_fetch_assoc($orderData)) {
                    $id = $rows["id"];
                    $title = $rows["food"];
                    $price = $rows["price"];
                    $quantity = $rows["qty"];
                    $name = $rows["customer_name"];
                    $email = $rows["customer_email"];
                    $number = $rows["customer_contact"];
                    $addrss = $rows["customer_address"];
                ?>
                    <tr>
                        <td>
                            <?php echo $sn++ ?>
                        </td>
                        <td>
                            <?php echo $title ?>
                        </td>
                        <td>
                            <?php echo $price ?>
                        </td>
                        <td>
                            <?php echo $quantity ?>
                        </td>
                        <td>
                            <?php echo $name ?>
                        </td>
                        <td>
                            <?php echo $email ?>
                        </td>
                        <td>
                            <?php echo $number ?>
                        </td>
                        <td>
                            <?php echo $addrss ?>
                        </td>
                        <td>
                            <a href="http://localhost/food-site/food-order-oop/view/update-food.php?id=<?php echo $id ?>" class='btn-secondary text-deco'>Update Order</a>
                            <a href="http://localhost/food-site/food-order-oop/app/controller/delete-food-controller.php?id=<?php echo $id ?>" class='btn-danger text-deco'>Delete Order</a>
                        </td>
                    </tr>
            <?php
                }
            ?>
        </table>
    </div>
</div>

<?php include "./partials/footer.php"; ?>