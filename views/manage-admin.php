<?php
    include "./partials/menu.php";
    include "../app/Controller/ManageAdminController.php";
    include "../helper/session-helper.php";
?>
<div class="main-content">
    <div class="wrapper">
        <h1>Managa Admin</h1>
        <br>
        <strong><?php echo flash("delete admin") ?></strong>
        <br>
        <a href="add-admin.php" class="btn-primary text-deco">Add Admin</a>
        <br><br>
        <table class="tbl-full">
            <tr>
                <th>S.N</th>
                <th>Full Name</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            <?php
                $displayAdmin = new ManageAdminController();
                $adminData = $displayAdmin->sendAdmin();
                $sn=1;
                // print_r($adminData);
                while ($rows = mysqli_fetch_assoc($adminData)) {
                    $id = $rows["id"];
                    $full_name = $rows["full_name"];
                    $user_name = $rows["user_name"];
                    $email = $rows["email"];
                ?>
                    <tr>
                        <td>
                            <?php echo $sn++ ?>
                        </td>
                        <td>
                            <?php echo $full_name ?>
                        </td>
                        <td>
                            <?php echo $user_name ?>
                        </td>
                        <td>
                            <?php echo $email ?>
                        </td>
                        <td>
                            <a href="http://localhost/food-site/food-order-oop/view/update-password.php?id=<?php echo $id ?>" class="btn-primary text-deco">Change Password</a>
                            <a href="http://localhost/food-site/food-order-oop/view/update-admin.php?id=<?php echo $id ?>" class='btn-secondary text-deco'>Update Admin</a>
                            <a href="http://localhost/food-site/food-order-oop/controller/DeleteAdminController.php?id=<?php echo $id ?>" class='btn-danger text-deco'>Delete Admin</a>
                        </td>
                    </tr>
            <?php
                }
            ?>
        </table>
    </div>
</div>
    <!-- using relative patha -->
<!-- absoulte , relative path (for route) -->
<?php include "./partials/footer.php"; ?>