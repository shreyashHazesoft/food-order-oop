<?php
include "../connection.php";

class FoodData extends Connection
{
    public function getFoodData()
    {
        $connect = $this->connect();
        $sql = "SELECT * FROM tbl_food;";
        $res = mysqli_query($connect, $sql);
        return $res;
    }
}